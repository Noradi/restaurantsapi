const express = require('express');
const router = express.Router();
const axios = require('axios')
const jwt = require('jsonwebtoken')
router.get('/', async function (req, res, next) {
    const token = req.headers.authorization.replace("Bearer ", "")
    const { name, id } = jwt.decode(token) || {name:'',id:''}
    const { NAME, ID } = process.env;
    if (NAME === name || ID === id) {
      const { latitude, longitude, radius, rangePrice, attendees, nextPageToken } = req.query;
     
     const baseUrl ="https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
     const location = `location=${latitude},${longitude}`;
     const perimeter = `radius=${radius}`;
     const API_KEY = process.env.GOOGLE_API_KEY;
     const maxPrice = `${rangePrice ? `&maxprice=${rangePrice}` : ""}`;
     const keywords = `${attendees ? `&keyword=${attendees}` : ""}`;
      const nextPage = `${nextPageToken ? `&pagetoken=${nextPageToken}` : ""}`;
     const fields = "fields=formatted_address,name";
     if (
       !latitude ||
       !longitude ||
       !radius ||
       isNaN(latitude) ||
       isNaN(longitude) ||
       isNaN(radius)
     ) {
       res.status(400).send({
         success: false,
         message:"The params `latitude, longitude and radius` is required and it should be a number",
         data: [],
       });
     } else if (
       (rangePrice && isNaN(rangePrice)) ||
       rangePrice < 0 ||
       rangePrice > 4
     ) {
       res.status(400).send({
         success: false,
         message: "Range price is a number and it should be 0,1,2,3 or 4",
         data: [],
       });
     } else {
       try {
         const { data } = await axios.get(`${baseUrl}${location}&${perimeter}&type=restaurant${keywords}${maxPrice}&${fields}&key=${API_KEY}${nextPage}`);
         res.status(200).send({
           success: true,
           message: "",
           data,
         });
       } catch (e) {
         res.status(500).send(e);
         console.log("error", e);
       }
     }
    } else {
       res.status(400).send({
         success: false,
         message: "Invalid credentials ! Please contact your administrator",
         data: [],
       });
    }

  
});

module.exports = router;
